%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Jacobs Landscape Poster
% LaTeX Template
% Version 1.1 (14/06/14)
%
% Created by:
% Computational Physics and Biophysics Group, Jacobs University
% https://teamwork.jacobs-university.de:8443/confluence/display/CoPandBiG/LaTeX+Poster
% 
% Further modified by:
% Nathaniel Johnston (nathaniel@njohnston.ca)
%
% This template has been downloaded from:
% http://www.LaTeXTemplates.com
%
% License:
% CC BY-NC-SA 3.0 (http://creativecommons.org/licenses/by-nc-sa/3.0/)
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%----------------------------------------------------------------------------------------
%	PACKAGES AND OTHER DOCUMENT CONFIGURATIONS
%----------------------------------------------------------------------------------------

\documentclass[9pt]{beamer}
\usepackage{subcaption} 
\usepackage[scale=1.05]{beamerposter} % Use the beamerposter package for laying out the poster

\usetheme{confposter} % Use the confposter theme supplied with this template

\setbeamercolor{block title}{fg=ngreen,bg=white} % Colors of the block titles
\setbeamercolor{block body}{fg=black,bg=white} % Colors of the body of blocks
\setbeamercolor{block alerted title}{fg=white,bg=dblue!70} % Colors of the highlighted block titles
\setbeamercolor{block alerted body}{fg=black,bg=dblue!10} % Colors of the body of highlighted blocks
% Many more colors are available for use in beamerthemeconfposter.sty

%-----------------------------------------------------------
% Define the column widths and overall poster size
% To set effective sepwid, onecolwid and twocolwid values, first choose how many columns you want and how much separation you want between columns
% In this template, the separation width chosen is 0.024 of the paper width and a 4-column layout
% onecolwid should therefore be (1-(# of columns+1)*sepwid)/# of columns e.g. (1-(4+1)*0.024)/4 = 0.22
% Set twocolwid to be (2*onecolwid)+sepwid = 0.464
% Set threecolwid to be (3*onecolwid)+2*sepwid = 0.708

\newlength{\sepwid}
\newlength{\onecolwid}
\newlength{\twocolwid}
\newlength{\threecolwid}
\setlength{\paperwidth}{48in} % A0 width: 46.8in
\setlength{\paperheight}{36in} % A0 height: 33.1in
\setlength{\sepwid}{0.024\paperwidth} % Separation width (white space) between columns
\setlength{\onecolwid}{0.22\paperwidth} % Width of one column
\setlength{\twocolwid}{0.464\paperwidth} % Width of two columns
\setlength{\threecolwid}{0.708\paperwidth} % Width of three columns
\setlength{\topmargin}{-0.5in} % Reduce the top margin size
%-----------------------------------------------------------

\usepackage{graphicx}  % Required for including images

\usepackage{booktabs} % Top and bottom rules for tables

%----------------------------------------------------------------------------------------
%	TITLE SECTION 
%----------------------------------------------------------------------------------------

\title{Multi-component Assembly of Microcompartments: the Driving Force} % Poster title

\author{Yaohua Li, Nolan Kennedy, Curt Waltmann, Danielle Tullman-Ercek, Monica Olvera de la Cruz} % Author(s)

\institute{Northwestern University} % Institution(s)

%----------------------------------------------------------------------------------------

\begin{document}

\addtobeamertemplate{block end}{}{\vspace*{2ex}} % White space under blocks
\addtobeamertemplate{block alerted end}{}{\vspace*{2ex}} % White space under highlighted (alert) blocks

\setlength{\belowcaptionskip}{2ex} % White space under figures
\setlength\belowdisplayshortskip{2ex} % White space under equations

\begin{frame}[t] % The whole poster is enclosed in one beamer frame

\begin{columns}[t] % The whole poster consists of three major columns, the second of which is split into two columns twice - the [t] option aligns each column's content to the top

\begin{column}{\sepwid}\end{column} % Empty spacer column

\begin{column}{\onecolwid} % The first column

%----------------------------------------------------------------------------------------
%	OBJECTIVES
%----------------------------------------------------------------------------------------

\begin{alertblock}{Abstract}

We study the assembly of Bacterial Micro-Compartment using atomistic molecular dynamics and coarse-grained models. Pdu microcompartments assemble from hexameric and pentameric building blocks with the help of a protein scaffold. The interaction energy between building blocks are quantitatively calculated in atomistic MD simulations by umbrella sampling method. We study the effect of electrostatics in virus-like particle assemblies. Preliminary results show that electrostatic interaction, including hydrogen bonds, contribute significantly to the interaction between building blocks.

\end{alertblock}

%----------------------------------------------------------------------------------------
%	INTRODUCTION
%----------------------------------------------------------------------------------------

\begin{block}{Introduction}

Bacterial microcompartments (BMC) are protein based structures that share very similar assembly mechanics to viral capsids. Many of them have quasi icosahedral symmetry, and have 100$\sim$200 nm characteristic length. 
Studying the pathway of BMCs using molecular dynamics (MD) is a promising yet challenging task due to their large size and complex energy landscape.
%Although BMCs and viral capsids have been studied for years, the physical mechanism of their formation, especially the bigger shells, remains unsolved. Molecular dynamics (MD) provides irreplaceable value for this problem because it grants us direct access to any intermediate state of the process, and allows us to calculate any order parameter of our interest. 


\begin{figure}
	\includegraphics[width=0.98\linewidth]{pdu_in_cell.png}
	\caption{Left: Pdu BMCs assembled \textit{in vivo} in salmonella cells. Right: Purified BMCs demonstrate a variety of polyhedral shapes, with faces much larger than the area of a single building block.}
	\label{figpdu}
\end{figure}

A complete Pdu BMC is made of 3 proteins: BMC hexamers, BMC trimers and BMC pentamers. Experiments suggest that the ratio of the 3 components affect the assembly product, and the stability of these products depend on the pH and salt conditions. The BMCs also enclose scaffold proteins, the role of which is still unclear.

\begin{figure}
	\begin{subfigure}[b]{0.33\textwidth}
		\includegraphics[width=\textwidth, trim=0 0 0 0, clip]{tubes.png}
		\label{fig:hbond}
	\end{subfigure}
	%
	\begin{subfigure}[b]{0.5\textwidth}
		\includegraphics[width=\textwidth, trim=0 0 0 0, clip]{3compo.png}
		\label{fig:pmf}
	\end{subfigure}
	\caption{(a) When PduA protein is overexpressed, long tubes instead of polyhedral BMCs are formed. (b) 3 types of BMC component proteins. We hypothesize that BMC hexamers form facets, trimers form ridges, and pentamers form the 12 necessary vertices for a closed shell. Figure adapted from ref \cite{sutter2017assembly}}
\end{figure}

Our goal is to build a multi-scale MD model with the least number of assumptions, and capture the essential driving force that make the assembly happen.

\end{block}

%------------------------------------------------



%----------------------------------------------------------------------------------------

\end{column} % End of the first column

\begin{column}{\sepwid}\end{column} % Empty spacer column

\begin{column}{\twocolwid} % Begin a column which is two columns wide (column 2)

\begin{columns}[t,totalwidth=\twocolwid] % Split up the two columns wide column

\begin{column}{\onecolwid}\vspace{-.6in} % The first column within column 2 (column 2.1)





%----------------------------------------------------------------------------------------
%	Main Column
%----------------------------------------------------------------------------------------

\begin{block}{Atomistic MD Simulations}

	
 In order to probe the essential interaction forces between the building blocks, we first perform Atomistic MD simulations. Two BMC hexamers are found to dock edge to edge because of 2 hydrogen bonding sites.


\begin{figure}
	\begin{subfigure}[b]{0.4\textwidth}
		\includegraphics[width=\textwidth, trim=10 20 10 20, clip]{Hbond_il.png}
		\label{fig:hbond}
	\end{subfigure}
	%
	\begin{subfigure}[b]{0.55\textwidth}
		\includegraphics[width=\textwidth, trim=10 15 10 12, clip]{pmf.pdf}
		\label{fig:pmf}
	\end{subfigure}
\caption{(a) snapshot of two PduA hexamers being restrained by a harmonic potential, which pulls the two hexamers slightly apart, breaking one of the hydrogen bonds on the dangling arginine side chains. In the red circle, an Arginine form hydrogen bonding with an asparagine. (b) Potential of mean force calculated by umbrella sampling and WHAM. The two runs show some difference, likely due to short sampling time (10 ns). The binding energy of two hexamer is 5.5~6 kCal/mol.}
\end{figure}


We perform atomistic umbrella sampling simulations to obtain the interaction strength of two proteins. Two runs, each containing 15 windows, are performed. The two runs demonstrate different pathways: in run 1, two hydrogen bonds break almost simultaneously. In run 2, one of the hydrogen bonds breaks at COM distance 6.95 nm. This demonstrates the pathway dependence. The total binding energy has an uncertainty of at least 0.5 kCal/mol. Simulations of other building blocks and the scaffolds are being run.


\end{block}

\begin{block}{Driving Forces}
With preliminary atomistic simulations, we hypothesize that hydrogen bonds, electrostatic force and van der Waals forces are the main driving force of the assembly.
The inner dielectric constant of protein is 6$\sim$7. To see the effect of this dielectric mismatch, we perform a simple simulation of ions near a low dielectric sphere, using our electrostatic solver. (see fig \ref{fig:dielectric}) 

\begin{figure}
	\begin{subfigure}[b]{0.48\textwidth}
		\includegraphics[width=\textwidth, trim=0 0 0 0, clip]{dielectricsphere.png}

	\end{subfigure}
	%
	\begin{subfigure}[b]{0.45\textwidth}
		\includegraphics[width=\textwidth, trim=0 0 0 0, clip]{divalent_sphere.png}
	\end{subfigure}
	\caption{(a) Schematic of dielectric sphere simulation. The result of monovalent ions is trivial, almost no difference is observed with/without dielectric mismatch, therefore not shown. (b) Divalent ion distribution near a low dielectric sphere. The multivalent cations are slightly expelled from the surface due to image-charge effect.}
	\label{fig:dielectric}
\end{figure}
These simulations suggest that dielectric mismatch leads to a weak depletion of multivalent ions near neutral proteins. For charged proteins, this effect is weaker. We can include the depletion with a short-range repulsion.

\end{block}

%----------------------------------------------------------------------------------------

\end{column} % End of column 2.1

\begin{column}{\onecolwid}\vspace{-.6in} % The second column within column 2 (column 2.2)

%----------------------------------------------------------------------------------------
%	METHODS
%----------------------------------------------------------------------------------------

\begin{block}{Coarse-graining}

It has been reported that the inside facets have a unique pattern of charges that depend on the BMC-T type \cite{sutter2017assembly}. We analyze the charge distribution at physiological pH using the pdb2pqr online solver \cite{dolinsky2004pdb2pqr} which calculates pKa using PROPKA. And then we build our CG models.
\begin{figure}
	\begin{subfigure}[b]{0.4\textwidth}
		\includegraphics[width=\textwidth, trim=10 20 10 180, clip]{negative_side.png}
		\caption{Inside}
%		\label{fig:charge1}
	\end{subfigure}
	%
	\begin{subfigure}[b]{0.4\textwidth}
		\includegraphics[width=\textwidth, trim=10 150 10 150, clip]{positive_side.png} 
		\caption{Outside}
%		\label{fig:charge2}
	\end{subfigure}
\begin{subfigure}[b]{0.4\textwidth}
	\includegraphics[width=\textwidth, trim=10 10 10 10, clip]{charge.pdf}
	\caption{Total charge of shell and cargo protein.}
	%		\label{fig:charge1}
\end{subfigure}
%
\begin{subfigure}[b]{0.4\textwidth}
	\includegraphics[width=\textwidth, trim=0 0 0 0, clip]{dipole.pdf}
	\caption{Electric dipole moment of the shell protein}
	%		\label{fig:charge2}
\end{subfigure}
\caption{Charge distribution and analysis of PduA hexamer proteins.}
\label{fig:charge_distri}
\end{figure}
% trim={<left><lower><right>}

\textbf{PALACE CG Forcefield} \cite{pasi2012palace}
A fine resolution CG model that can represent the secondary and tertiary structure of proteins while still allowing them to be deformable in the presence of an outside stimulus. Water is implicit, greatly reducing the number of atoms in simulations. It is combined with the CVCEL model for electrostatics\cite{ceres2015improving}, which fits the effective dielectric constant to the circuclar variance (CV) of the interacting charges. In this way it takes into account the effect of protein deformation on the electrostatic interactions in the system. This model is still too cumbersome to model the assembly of microcompartments.

\textbf{Shape-based Model}
To reproduce the formation of complete MCPs, we adopt a shape-based coarse-grained method. Each bead represents 50$\sim$70 atoms, and has their total charge. Due to the critical importance of the 6 arginine "hands", it is enforced that one bead represent the "hand" and it carries one proton charge. As a preliminary result, the two CG proteins indeed form edge to edge dimers as expected from full atom simulations. Fig. \ref{fig:cg_dimer}


\begin{figure}
	\begin{subfigure}[b]{0.4\textwidth}
		\includegraphics[width=\textwidth, trim=2 2 2 2, clip]{3ngk_cv.png}
		\caption{}
		\label{fig:cg_dimer}
	\end{subfigure}
	%
	\begin{subfigure}[b]{0.4\textwidth}
		\includegraphics[width=\textwidth, trim=10 50 10 10, clip]{dimer.png}
		\caption{}
		\label{fig:2}
	\end{subfigure}
\caption{(a) The PduA hexamer in the PALACE representation. (b) Two PduA hexamer in the shape based CG representation.}
\end{figure}

\end{block}

%----------------------------------------------------------------------------------------

\end{column} % End of column 2.2

\end{columns} % End of the split of column 2 - any content after this will now take up 2 columns width


%----------------------------------------------------------------------------------------

\begin{columns}[t,totalwidth=\twocolwid] % Split up the two columns wide column again

\begin{column}{\onecolwid} % The first column within column 2 (column 2.1)



%----------------------------------------------------------------------------------------

\end{column} % End of column 2.1

\begin{column}{\onecolwid} % The second column within column 2 (column 2.2)

%----------------------------------------------------------------------------------------
%	RESULTS
%----------------------------------------------------------------------------------------


%----------------------------------------------------------------------------------------

\end{column} % End of column 2.2

\end{columns} % End of the split of column 2

\end{column} % End of the second column

\begin{column}{\sepwid}\end{column} % Empty spacer column

\begin{column}{\onecolwid} % The third column


%----------------------------------------------------------------------------------------
%	ADDITIONAL INFORMATION
%----------------------------------------------------------------------------------------

\begin{block}{Future Work}

Due to the complex shape of the building blocks, it is difficult to assemble the MCP from random initial configurations. We plan to initialize the system from a few shapes anticipated from experiments. The role of scaffold will be studied by varying the amount of scaffold proteins.


\begin{figure}
	\includegraphics[width=0.66\linewidth,trim=2 0 0 0, clip]{shape.png}
	\caption{Possible assembled shape.}
	\label{fig:shape}
\end{figure}

	
\end{block}



%----------------------------------------------------------------------------------------
%	CONCLUSION
%----------------------------------------------------------------------------------------

\begin{block}{Conclusions}
Pdu microcompartments assemble with the help of hydrogen bonds. A binding energy of $\sim$ 5.5 kCal/mol is calculated umbrella sampling simulations. 
Charge distribution and electrostatic interactions plays an important role in capsid assembly. By using a shape-based coarse grain model with Lenard-Jones potential and screened electrostatic potential, we are able to reproduce a dimer of the assembly.

\end{block}


%----------------------------------------------------------------------------------------
%	REFERENCES
%----------------------------------------------------------------------------------------

\begin{block}{References}

%\nocite{} % Insert publications even if they are not cited in the poster
\small{\bibliographystyle{unsrt}
\bibliography{sample}\vspace{0.75in}}

\end{block}

%----------------------------------------------------------------------------------------
%	ACKNOWLEDGEMENTS
%----------------------------------------------------------------------------------------

\setbeamercolor{block title}{fg=red,bg=white} % Change the block title color

\begin{block}{Acknowledgements}

\small{\rmfamily{The authors thank Dr. Baofu Qiao, Dr. Trung Nguyen for helpful discussions. This work is supported by Sherman Fairchild Foundation.}} \\

\end{block}

%----------------------------------------------------------------------------------------
%	CONTACT INFORMATION
%----------------------------------------------------------------------------------------

\setbeamercolor{block alerted title}{fg=black,bg=norange} % Change the alert block title colors
\setbeamercolor{block alerted body}{fg=black,bg=white} % Change the alert block body colors


\begin{center}
\begin{tabular}{ccc}
 \hfill & \includegraphics[width=0.4\linewidth]{logo.png}
\end{tabular}
\end{center}

%----------------------------------------------------------------------------------------

\end{column} % End of the third column

\end{columns} % End of all the columns in the poster

\end{frame} % End of the enclosing frame

\end{document}
